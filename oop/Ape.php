<?php

require_once 'Animal.php';

class Ape extends Animal {
    public function yell() {
        echo "Auooo";
    }

    public function getLegs() {
        return 2;
    }
}
