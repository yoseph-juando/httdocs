<?php

require_once 'Animal.php';

class Frog extends Animal {
    public function jump() {
        echo "hop hop";
    }

    public function getColdBlooded() {
        return "yes";
    }
}
