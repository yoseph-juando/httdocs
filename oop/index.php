<?php

require_once 'Ape.php';
require_once 'Frog.php';
require_once 'Animal.php';

$shaun = new 'Animal' ("shaun");
echo "Nama: " . $shaun->name . "<br>";
echo "Jumlah kaki: " . $shaun->getLegs() . "<br>";
echo "Cold-blooded: " . $shaun->getColdBlooded() . "<br>";
echo "<br>";

$sungokong = new 'Ape'("kera sakti");
$sungokong->yell("Auooo");
echo "<br>";

$kodok = new 'Frog'("buduk");
$kodok ->jump("Hop Hop");

